﻿using OpenQA.Selenium;

namespace BBCNewsAutomationFramework.WebElements
{
   public class FootballMatchWebElement
    {
        private readonly IWebElement element;

        public FootballMatchWebElement(IWebElement element)
        {
            this.element = element;
        }

        public string GetFirstTeam()
        {
            return element.FindElement(By.XPath(".//span[contains(@class,'team-name--home')]")).Text;
        }

        public string GetSecondTeam()
        {
            return element.FindElement(By.XPath(".//span[contains(@class,'team-name--away')]")).Text;
        }

        public string GetFirstScore()
        {
            return element.FindElement(By.XPath(".//span[contains(@class,'number--home')]")).Text;
        }

        public string GetSecondScore()
        {
            return element.FindElement(By.XPath(".//span[contains(@class,'number--away')]")).Text;
        }

        public void GoToMatchPage() => element.Click();
    }
}