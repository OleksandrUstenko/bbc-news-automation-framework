﻿using BBCNewsAutomationFramework.Steps.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace BBCNewsAutomationFramework
{
    [Binding]
    public class StepArgumentTransformation
    {
        [StepArgumentTransformation]
        public List<string> TransformTableToList(Table table) => table.Rows.Select(row => row.Values.First()).ToList();
        
        [StepArgumentTransformation]
        public CredentialKey TransformStringToCredetialKey(string credential)
        {
            switch (credential)
            {
                case "Valid credentials": return CredentialKey.ValidCredentials;

                case "Credentials without name": return CredentialKey.CredentialsWithoutName;

                case "Credentials without acceptance age": return CredentialKey.CredentialsWithoutAcceptanceAge;

                case "Credentials without acceptance user terms": return CredentialKey.CredentialsWithoutAcceptanceUserTerms;

                case "Credentials without required fields": return CredentialKey.CredentialsWithoutRequiredFields;

                default: throw new Exception("Unable to transform to credential key.");
            }
        }   
    }
}