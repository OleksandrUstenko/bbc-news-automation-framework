﻿Feature: Part 1
	
Scenario: Check the name of the headline article
	Given I go to 'News' tab on the main page 
	Then I see the main article with 'Kim apologises for killing of South Korean official' title

Scenario: Check the names of the secondary articles
	Given I go to 'News' tab on the main page 
	Then I see the secondary articles with titles
	| Titles                                          |
	| Cardinal forced out in rare Vatican resignation |
	| Two hurt in stabbing near Charlie Hebdo office  |
	| Ginsburg becomes first US woman to lie in state |
	| Are the lockdown restrictions too strict?       |
	| 'Body positivity has been commercialised'       |

Scenario: Check the name of the first article at search results page
	Given I go to 'News' tab on the main page 
	And I remember the category of headline article
	When I search by remembered category name
	Then I see the first article with 'Asia weather forecast' title