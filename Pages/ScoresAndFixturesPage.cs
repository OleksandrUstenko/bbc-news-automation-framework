﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BBCNewsAutomationFramework.Pages
{
   public class ScoresAndFixturesPage : BasePage
    {
        public ScoresAndFixturesPage(IWebDriver driver) : base(driver) { }

        private IWebElement SearchTextField => driver.FindElement(By.XPath("//input[@name='search']"));

        private IEnumerable<IWebElement> ResultsList => driver.FindElements(By.XPath("//a[contains(@class,'result')]"));

        public void SetSearchingtext(string text)
        {
            SearchTextField.SendKeys(text);
            ResultsList.First(s=>s.Text.Equals(text)).Click();
            WaitForPageLoadComplete();
        }
    }
}