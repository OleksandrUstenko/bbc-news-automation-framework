﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace BBCNewsAutomationFramework.Pages
{
    public class SearchResultsPage : BasePage
    {
        public SearchResultsPage(IWebDriver driver) : base(driver) { }

        private IEnumerable<IWebElement> ArticlesLabelesList => driver.FindElements(By.XPath("//p[contains(@class,'PromoHeadline')]"));

        public string GetTheTitleOfTheFirstArticle() => ArticlesLabelesList.First().Text;
    }
}