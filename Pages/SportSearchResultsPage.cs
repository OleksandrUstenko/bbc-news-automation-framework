﻿using BBCNewsAutomationFramework.WebElements;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BBCNewsAutomationFramework.Pages
{
    public class SportSearchResultsPage : BasePage
    {
        public SportSearchResultsPage(IWebDriver driver) : base(driver) { }

        private IEnumerable<IWebElement> MonthList => driver.FindElements(By.XPath("//div[@class='sp-c-content-slider__scroller']//li[contains(@class,'timeline')]"));

        private IEnumerable<FootballMatchWebElement> MatchList => driver.FindElements(By.XPath("//div[@class='sp-c-fixture__wrapper']")).Select(item => new FootballMatchWebElement(item));

        public void SelectMonth(string month)
        {
            MonthList.First(s => s.Text.Contains(month)).Click();
        }

        public FootballMatchWebElement GetMatchForTeams(string firstTeam, string secondTeam)
        {
           return MatchList.First(i => i.GetFirstTeam().Equals(firstTeam) && i.GetSecondTeam().Equals(secondTeam));
        }
        public void GoToMatch(string firstTeam, string secondTeam)
        {
            GetMatchForTeams(firstTeam,secondTeam).GoToMatchPage();
            WaitForPageLoadComplete();
        }

        public (string firstScore,string secondScore) GetMatchScoreForTeams(string firstTeam, string secondTeam)
        {
            FootballMatchWebElement match = GetMatchForTeams(firstTeam, secondTeam);
            return (match.GetFirstScore(), match.GetSecondScore());
        }
    }  
}