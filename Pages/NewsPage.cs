﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace BBCNewsAutomationFramework.Pages
{
    public class NewsPage : BasePage
    {
        public NewsPage(IWebDriver driver) : base(driver) { }
        
        private IWebElement HeadlineArticleLabel => driver.FindElement(By.XPath("//div[@data-entityid='container-top-stories#1']" +
            "//div[not(contains(@class,'none@m'))]/div/a[contains(@class,'promo-heading')]"));

        private IEnumerable<IWebElement> SecondaryArticlesLabelesList => 
            driver.FindElements(By.XPath("//div[contains(@class,'top-stories__secondary')]//a[contains(@class,'promo-heading')]"));

        private IWebElement HeadlineArticleCategory => driver.FindElement(By.XPath("//div[@data-entityid='container-top-stories#1']" +
            "//ul[contains(@class,'gel-brevier')]//a[contains(@class,'section-link')]"));

        private IEnumerable<IWebElement> NewsTabs => driver.FindElements(By.XPath("//ul[contains(@class,'wide-sections')]//span"));

        public string GetHeadlineArticleText() => HeadlineArticleLabel.Text;
      
        public List<string> GetSecondaryArticlesLabelesListText() => SecondaryArticlesLabelesList.Select(element => element.Text).ToList();

        public string GetHeadlineArticleCategoryText() => HeadlineArticleCategory.Text;

        public void GoToNewsTab(string tabName)
        {
            NewsTabs.First(s => s.Text.Equals(tabName)).Click();
            WaitForPageLoadComplete();
        }
    }
}