﻿using OpenQA.Selenium;

namespace BBCNewsAutomationFramework.Pages
{
    public class HomePage : BasePage
    {
        public HomePage(IWebDriver driver): base(driver) { } 
    }
}