﻿using OpenQA.Selenium;

namespace BBCNewsAutomationFramework.Pages
{
    public class CoronavirusPage : BasePage
    {
        public CoronavirusPage(IWebDriver driver) : base(driver) { }

        private IWebElement YourCoronavirusStoriesButton => driver.FindElement(By.XPath("//nav[contains(@class,\"wide-secondary\")]" +
            "//span[contains(text(),\"Your Coronavirus Stories\")]"));
        public void GoToYourCoronavirusStoriesTab() 
        {
            YourCoronavirusStoriesButton.Click();
            WaitForPageLoadComplete();
        }
    }
}