﻿using TechTalk.SpecFlow;

namespace BBCNewsAutomationFramework.Steps
{
    [Binding]
    public  class GeneralSteps : BaseSteps
    {
        [Given(@"I go to '(.*)' tab on the main page")]
        public void GivenIGoToTabOnTheMainPage(string tabName)
        {
            homePage.GoToNavigationTab(tabName);
            homePage.CloseSignInPopupIfNeeded();
        }
    }
}