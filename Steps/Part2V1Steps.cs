﻿using BBCNewsAutomationFramework.Steps.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;

namespace BBCNewsAutomationFramework.Steps
{
    [Binding]
    public class Part2V1Steps : BaseSteps
    {
        [Given(@"I go to '(.*)' news tab")]
        public void GivenIGoToNewsTab(string tabName) => newsPage.GoToNewsTab(tabName);

        [Given(@"I go to Your Coronavirus Stories tab on coronavirus page")]
        public void GivenIGoToYourCoronavirusStoriesTabOnCoronavirusPage() => coronavirusPage.GoToYourCoronavirusStoriesTab();

        [Given(@"I go to '(.*)' on your coronavirus stories page")]
        public void GivenIGoToOnYourCoronavirusStoriesPage(string tabName) => yourCoronavirusStoriesPage.GoToGetInTouchTab(tabName);

        [When(@"I submit '(.*)' credentials with '(.*)' article text")]
        public void WhenISubmitCredentialsWithArticleText(CredentialKey credentialsKey, string articleText) =>
            howToSharePage.SubmitCredentialsAndArticleText(CredentialManager.Credentials[credentialsKey], articleText);

        [Then(@"I see an '(.*)' error message")]
        public void ThenISeeAnErrorMessage(string errorMessages)
        {
            List<string> expectedMessages = errorMessages.Split(",").ToList();
            List<string> actualMessages = howToSharePage.GetErrorMessages();
            CollectionAssert.AreEquivalent(expectedMessages, actualMessages, "Error message is not displayed");
        }
    }
}