﻿namespace BBCNewsAutomationFramework.Steps.Data
{
   public enum CredentialKey
    {
        ValidCredentials,
        CredentialsWithoutName,
        CredentialsWithoutAcceptanceAge,
        CredentialsWithoutAcceptanceUserTerms,
        CredentialsWithoutRequiredFields
    }
}
