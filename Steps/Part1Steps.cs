﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace BBCNewsAutomationFramework.Steps
{
    [Binding]
    public class Part1Steps : BaseSteps
    {
        private string rememberedCategory;

        [Given(@"I remember the category of headline article")]
        public void ThenIRememberTheCategoryOfHeadlineArticle() => rememberedCategory = newsPage.GetHeadlineArticleCategoryText();

        [When(@"I search by remembered category name")]
        public void ISearchByRememberedCategoryName() => homePage.SetSearchText(rememberedCategory);

        [Then(@"I see the main article with '(.*)' title")]
        public void ThenISeeTheMainArticleWithTitle(string value) => Assert.AreEqual(value, newsPage.GetHeadlineArticleText(), "Headline title is not the same as expected!");

        [Then(@"I see the secondary articles with titles")]
        public void ThenISeeTheSecondaryArticlesWithTitles(List<string> expectedValues)
        {
            CollectionAssert.AreEqual(expectedValues, newsPage.GetSecondaryArticlesLabelesListText(), "Secondary titles are not the same as expected!");
        }
       
        [Then(@"I see the first article with '(.*)' title")]
        public void ThenISeeTheFirstArticleWithTitle(string value) =>
            Assert.AreEqual(value, searchResultsPage.GetTheTitleOfTheFirstArticle(), "Headline title is not the same as expected!");
    }
}