﻿using TechTalk.SpecFlow;

namespace BBCNewsAutomationFramework.Steps
{
   [Binding]
   public class SpecFlowActions : BaseSteps
    {
        [BeforeScenario]
        public void InitTest() => Driver.Navigate().GoToUrl("https://www.bbc.com");

        [AfterScenario]
        public void Cleanup()
        {
            Driver.Quit();
            Driver = null;
        }
    }
}