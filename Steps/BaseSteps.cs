﻿using BBCNewsAutomationFramework.Pages;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.IO;
using System.Reflection;

namespace BBCNewsAutomationFramework
{
    public class BaseSteps
    {       
        private static IWebDriver driver;

        protected static IWebDriver Driver
        {
            get
            {
                if (driver == null)
                {
                    ChromeOptions options = new ChromeOptions();
                    options.AddArguments("--start-maximized");
                    driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),options);
                }
                return driver;
            }
            set => driver = value;
        }

        public HomePage homePage = new HomePage(Driver);

        public NewsPage newsPage = new NewsPage(Driver);

        public SearchResultsPage searchResultsPage = new SearchResultsPage(Driver);

        public CoronavirusPage coronavirusPage = new CoronavirusPage(Driver);

        public YourCoronavirusStoriesPage yourCoronavirusStoriesPage = new YourCoronavirusStoriesPage(Driver);

        public HowToSharePage howToSharePage = new HowToSharePage(Driver);

        public SportPage sportPage = new SportPage(Driver);

        public FootballPage footballPage = new FootballPage(Driver);

        public ScoresAndFixturesPage scoresAndFixturesPage = new ScoresAndFixturesPage(Driver);

        public SportSearchResultsPage sportSearchResultsPage = new SportSearchResultsPage(Driver);

        public MatchPage matchPage = new MatchPage(Driver);
    }
}